package com.labs;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.List;

public class Gui extends JFrame implements ActionListener {

    Label path, pathFeatures;
    Button buttonSubmit, buttonExit;
    TextField textField, textFieldFeatures;

    Gui() {
        System.setProperty("submit", "#16A91D");
        System.setProperty("exit", "#1690A9");

        Font colorFont = new Font("Serif", Font.BOLD, 15);
        path = new Label("Please enter full path of plans.txt file, eg D:\\TelepathyLabs\\plans.txt");
        path.setBounds(50, 50, 1000, 20);
        textField = new TextField();
        textField.setBounds(50, 75, 1000, 20);
        add(path);
        add(textField);

        pathFeatures = new Label("Please enter the features needed separated by comma (,)");
        pathFeatures.setBounds(50, 100, 1000, 20);
        textFieldFeatures = new TextField();
        textFieldFeatures.setBounds(50, 125, 1000, 20);
        add(pathFeatures);
        add(textFieldFeatures);

        buttonSubmit = new Button("SUBMIT");
        buttonSubmit.setBounds(50, 150, 300, 40);
        buttonSubmit.setBackground(Color.getColor("submit"));
        buttonSubmit.setForeground(Color.WHITE);
        buttonSubmit.setFont(colorFont);
        buttonSubmit.addActionListener(this);
        add(buttonSubmit);

        buttonExit = new Button("EXIT");
        buttonExit.setBounds(425, 150, 300, 40);
        buttonExit.setBackground(Color.getColor("exit"));
        buttonExit.setForeground(Color.WHITE);
        buttonExit.setFont(colorFont);
        buttonExit.addActionListener(this);
        add(buttonExit);

        setSize(1200, 400);
        setLayout(null);
        setLocationRelativeTo(null);
        setVisible(true);
        setTitle("TELEPATHY LABS");
        Image icon = Toolkit.getDefaultToolkit().getImage("C:\\LabBestPlan\\src\\main\\java\\com\\labs\\telepathyLabs.PNG");
        setIconImage(icon);
    }

    public void actionPerformed(ActionEvent e1) {

        if (e1.getActionCommand().equalsIgnoreCase("SUBMIT")) {

            System.out.println("textField.getText(): " + textField.getText());
            if (textField.getText().equalsIgnoreCase("")) {
                JOptionPane.showMessageDialog(null, "Looks like you missed to enter the path. Try Again.");
            } else {
                Scanner s = null;
                try {
                    s = new Scanner(new File(textField.getText()));
                } catch (FileNotFoundException e) {
                    JOptionPane.showMessageDialog(null, "Path invalid. Try again");
                }
                List<String> listOfPlans = new ArrayList<>();
                while (true) {
                    assert s != null;
                    if (!s.hasNext()) break;
                    listOfPlans.add(s.next());
                }
                s.close();
                System.out.println("listOfPlans"+listOfPlans);

                if (textFieldFeatures.getText().isEmpty()){
                    JOptionPane.showMessageDialog(null, "Looks like you missed to enter the features. Try Again.");
                }
                else{

                List<String> neededFeature = new ArrayList<>(Arrays.asList(textFieldFeatures.getText().split(",")));

                List<String> featureList1;
                List<PlanDetails> planList = new ArrayList<>();
                for (int i=0;i<=listOfPlans.size()-1;i++) {
                    // Split first plan in list and add feature in the list
                    System.out.println("plans one by one: "+listOfPlans.get(i));
                    String[] split = listOfPlans.get(i).split(",");
                    System.out.println("split.length: " + split.length);
                    featureList1 = new ArrayList<>(Arrays.asList(split).subList(2, split.length));
                    System.out.println("featureList1: "+featureList1);
                    planList.add(new PlanDetails(split[0], Integer.valueOf(split[1]), featureList1));
                }
                // Data Structure for 3 types
                HashMap<String, List<String>> featurePlans = new HashMap<>();
                Map<String, List<String>> planFeatureMap = new HashMap<>();
                Map<String, Integer> planPriceMap = new HashMap<>();

                for (PlanDetails planDetails : planList) {
                    String planName = planDetails.getPlanName();
                    planFeatureMap.put(planName, planDetails.getPlanFeature());
                    planPriceMap.put(planName, planDetails.getPlanPrice());
                    for (String featureName : planDetails.getPlanFeature()) {
                        if (neededFeature.contains(featureName)) {
                            List<String> planNames;
                            if (featurePlans.containsKey(featureName)) {
                                planNames = featurePlans.get(featureName);

                            } else {
                                planNames = new ArrayList<>();
                            }
                            planNames.add(planName);
                            featurePlans.put(featureName, planNames);
                        }
                    }
                }
                System.out.println("Feature map size :" + featurePlans.size());

                Map<Integer, List<String>> finalPlanNames = new TreeMap<>();

                for (String key : featurePlans.keySet()) {
                    List<String> combinedFeatureList = new ArrayList<>();
                    String planNames = "";
                    int combinedPrice = 0;
                    List<String> featureList;
                    for (String planName : featurePlans.get(key)) {
                        combinedFeatureList.addAll(planFeatureMap.get(planName));
                        planNames = planNames.equals("") ? planName :  planNames +","+planName;
                        combinedPrice = combinedPrice + planPriceMap.get(planName);

                    }
                    if(combinedFeatureList.containsAll(neededFeature)) {
                        featureList = new ArrayList<>();
                        featureList.add(planNames);
                        finalPlanNames.put(combinedPrice,featureList);
                    }
                }
                System.out.println("Final Plan Names :"+finalPlanNames);

                try {
                    Map.Entry<Integer, List<String>> entry = finalPlanNames.entrySet().iterator().next();
                    Integer key = entry.getKey();
                    List<String> value = entry.getValue();
                    String formatted = value.toString()
                            .replace("[", "")
                            .replace("]", "");
                    System.out.println(key + "," + formatted);
                    JOptionPane.showMessageDialog(null, key + "," + formatted);
                }
                catch (Exception exception){
                    JOptionPane.showMessageDialog(null, "-1 : No matches found. Try Again.");

                }
            } }
        }
        else if (e1.getActionCommand().equalsIgnoreCase("EXIT")) {
                System.exit(1);
            }

        }
    static class PlanDetails {
        private final String planName;
        private final Integer planPrice;
        private final List<String> planFeature;

        public PlanDetails(String planName, Integer planPrice, List<String> planFeature) {
            super();
            this.planName = planName;
            this.planPrice = planPrice;
            this.planFeature = planFeature;
        }

        public String getPlanName() {
            return planName;
        }

        public Integer getPlanPrice() {
            return planPrice;
        }

        public List<String> getPlanFeature() {
            return planFeature;
        }

    }

    }
