///**
// *
// */
//package com.labs;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @author DELL
// *
// */
//public class PlanCompare1 {
//
//    /**
//     * @param args
//     */
//    public static void main(String[] args) {
//        // TODO Auto-generated method stub
//
//        List<PlanDetails1> planList = new ArrayList<>();
//        planList.add(new PlanDetails1("Plan1", 100, Arrays.asList("voice", "email")));
//        planList.add(new PlanDetails1("Plan2", 150, Arrays.asList("email", "database", "admin")));
//        planList.add(new PlanDetails1("Plan3", 125, Arrays.asList("voice", "admin")));
//        planList.add(new PlanDetails1("Plan4", 135, Arrays.asList("database", "admin")));
//        planList.add(new PlanDetails1("Plan5", 125, Arrays.asList("database", "admin","dba")));
//
//        List<String> neededFeature = Arrays.asList("email", "voice", "admin");
//
//        Map<String, List<String>> featurePlans = new HashMap<>();
//        Map<String, List<String>> planFeatureMap = new HashMap<>();
//        Map<String, Integer> planPriceMap = new HashMap<>();
//
//        for (PlanDetails1 planDetails1 : planList) {
//            String planName = planDetails1.getPlanName();
//            planFeatureMap.put(planName, planDetails1.getPlanFeature());
//            planPriceMap.put(planName, planDetails1.getPlanPrice());
//            for (String featureName : planDetails1.getPlanFeature()) {
//                if (neededFeature.contains(featureName)) {
//                    List<String> planNames;
//                    if (featurePlans.containsKey(featureName)) {
//                        planNames = featurePlans.get(featureName);
//
//                    } else {
//                        planNames = new ArrayList<>();
//                    }
//                    planNames.add(planName);
//                    featurePlans.put(featureName, planNames);
//                }
//            }
//        }
//        System.out.println("Feature map size :" + featurePlans.size());
//
//        List<String> finalPlanNames = new ArrayList<>();
//
//        for (String key : featurePlans.keySet()) {
//            List<String> combinedFeatureList = new ArrayList<>();
//            String planNames = "";
//            Integer combinedPrice = 0;
//            for (String planName : featurePlans.get(key)) {
//                combinedFeatureList.addAll(planFeatureMap.get(planName));
//                planNames = planNames.equals("") ? planName :  planNames +","+planName;
//                combinedPrice = combinedPrice + planPriceMap.get(planName);
//
//            }
//            if(combinedFeatureList.containsAll(neededFeature)) {
//                finalPlanNames.add(combinedPrice + " , "+planNames);
//            }else {
//                System.out.println( " All feature are not present ");
//            }
//        }
//
//        System.out.println(" Final Plan Names :"+finalPlanNames);
//    }
//
//}
//
//class PlanDetails1 {
//    private String planName;
//    private Integer planPrice;
//    private List<String> planFeature;
//
//    public PlanDetails1(String planName, Integer planPrice, List<String> planFeature) {
//        super();
//        this.planName = planName;
//        this.planPrice = planPrice;
//        this.planFeature = planFeature;
//    }
//
//    public String getPlanName() {
//        return planName;
//    }
//
//    public void setPlanName(String planName) {
//        this.planName = planName;
//    }
//
//    public Integer getPlanPrice() {
//        return planPrice;
//    }
//
//    public void setPlanPrice(Integer planPrice) {
//        this.planPrice = planPrice;
//    }
//
//    public List<String> getPlanFeature() {
//        return planFeature;
//    }
//
//    public void setPlanFeature(List<String> planFeature) {
//        this.planFeature = planFeature;
//    }
//
//}