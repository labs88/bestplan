//package com.labs;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.util.*;
//
//public class PlanCompare {
//
//    public static void main(String[] args) {
//        // Get the file
//        Scanner s = null;
//        try {
//            s = new Scanner(new File("D:\\TelepathyLabs\\PLANS\\plans.txt"));
//        } catch (FileNotFoundException e) {
//            System.out.println("File not found.");
////            JOptionPane.showMessageDialog(null, "Path invalid. Try again");
//        }
//        ArrayList<String> listOfPlans = new ArrayList<String>();
//        while (true) {
//            assert s != null;
//            if (!s.hasNext()) break;
//            listOfPlans.add(s.next());
//        }
//        s.close();
//        System.out.println("listOfPlans"+listOfPlans);
//        //      Features needed
//        List<String> neededFeature = Arrays.asList("voice", "email", "admin");
//
//        //populate the class plan details with txt file
//
//        List<String> featureList1;
//        List<PlanDetails> planList = new ArrayList<>();
//        for (int i=0;i<=listOfPlans.size()-1;i++) {
//            featureList1 = new ArrayList<>();
//            // Split first plan in list and add feature in the list
//            System.out.println("plans one by one: "+listOfPlans.get(i));
//            String[] split = listOfPlans.get(i).split(",");
//            System.out.println("split.length: " + split.length);
//            for (int a = 2; a <= split.length - 1; a++) {
//                featureList1.add(split[a]);
//            }
//            System.out.println("featureList1: "+featureList1);
//            planList.add(new PlanDetails(split[0], Integer.valueOf(split[1]), featureList1));
//        }
//
//        // Data Structure for 3 types
//        Map<String, List<String>> featurePlans = new HashMap<>();
//        Map<String, List<String>> planFeatureMap = new HashMap<>();
//        Map<String, Integer> planPriceMap = new HashMap<>();
//
//        for (PlanDetails planDetails : planList) {
//            String planName = planDetails.getPlanName();
//            planFeatureMap.put(planName, planDetails.getPlanFeature());
//            planPriceMap.put(planName, planDetails.getPlanPrice());
//            for (String featureName : planDetails.getPlanFeature()) {
//                if (neededFeature.contains(featureName)) {
//                    List<String> planNames;
//                    if (featurePlans.containsKey(featureName)) {
//                        planNames = featurePlans.get(featureName);
//
//                    } else {
//                        planNames = new ArrayList<>();
//                    }
//                    planNames.add(planName);
//                    featurePlans.put(featureName, planNames);
//                }
//            }
//        }
//        System.out.println("Feature map size :" + featurePlans.size());
//
//        Map<Integer, List<String>> finalPlanNames = new TreeMap<>();
//
//        for (String key : featurePlans.keySet()) {
//            List<String> combinedFeatureList = new ArrayList<>();
//            String planNames = "";
//            int combinedPrice = 0;
//            List<String> featureList;
//            for (String planName : featurePlans.get(key)) {
//                combinedFeatureList.addAll(planFeatureMap.get(planName));
//                planNames = planNames.equals("") ? planName :  planNames +","+planName;
//                combinedPrice = combinedPrice + planPriceMap.get(planName);
//
//            }
//            if(combinedFeatureList.containsAll(neededFeature)) {
//                featureList = new ArrayList<>();
//                featureList.add(planNames);
//                finalPlanNames.put(combinedPrice,featureList);
//            }else {
//                System.out.println( "All feature are not present ");
//            }
//        }
//
//        System.out.println("Final Plan Names :"+finalPlanNames);
//
//        Map.Entry<Integer, List<String>> entry = finalPlanNames.entrySet().iterator().next();
//        Integer key= entry.getKey();
//        List<String> value=entry.getValue();
//        String formatted = value.toString()
//                .replace("[","")
//                .replace("]","");
//        System.out.println(key+","+formatted);
//
//    }
//}
//
//class PlanDetails {
//    private String planName;
//    private Integer planPrice;
//    private List<String> planFeature;
//
//    public PlanDetails(String planName, Integer planPrice, List<String> planFeature) {
//        super();
//        this.planName = planName;
//        this.planPrice = planPrice;
//        this.planFeature = planFeature;
//    }
//
//    public String getPlanName() {
//        return planName;
//    }
//
//    public void setPlanName(String planName) {
//        this.planName = planName;
//    }
//
//    public Integer getPlanPrice() {
//        return planPrice;
//    }
//
//    public void setPlanPrice(Integer planPrice) {
//        this.planPrice = planPrice;
//    }
//
//    public List<String> getPlanFeature() {
//        return planFeature;
//    }
//
//    public void setPlanFeature(List<String> planFeature) {
//        this.planFeature = planFeature;
//    }
//
//}